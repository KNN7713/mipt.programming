#include <stdio.h>

double KahanSum(double * x, int n){
	double sum = 0.0, c = 0.0, y = 0.0, t = 0.0;
	int i;
	for (i = 0; i < n; ++ i){
		y = x[i] - c;
		t = sum + y;
		c = (t - sum) - y;
		sum = t;
	}
	return sum;
}

int main(){
	int n;
	scanf("%d", &n);
	int i;
	double x[10000], sum = 0;
	for (i = 0; i < n; ++ i){
		scanf("%lf", &x[i]);
		sum += x[i];
	}
	double K_sum = KahanSum(x, n);
	printf("Usually summation = %.30lf\n", sum);
	printf("Compensatory summation = %.30lf\n", K_sum);
	return 0;
}
	
