#include <stdio.h>
#include <stdlib.h>

int cur;

void print_bytes(const void * mem, int size){
	int j = size - 1;
	for (j; j >= 0; -- j)
		printb(*((char*)mem + j));
	printf("\n");
}

void printb(char a){
	int i = 7;
	for(i; i >= 0; --i)
		if (a&(1 << i))
			printf("1");
		else
			printf("0");
	cur ++;
	if (cur == 1 || cur == 9)
		printf(" ");
}

int main(){
	double x;
	scanf("%lf", &x);
	print_bytes(&x, sizeof(double));
	float a = x;
	print_bytes(&a, sizeof(float));
	
	int b = x;
	print_bytes(&b, sizeof(int));
	
	unsigned int c = x;
	print_bytes(&c, sizeof(unsigned int));
	return 0;
}
