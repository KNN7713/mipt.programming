#include <stdio.h>
#include <math.h>

typedef struct point{
	int x, y;
};

double dist(struct point a, struct point b){
	return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

int main(){
	int n;
	printf("Enter the number of points on the plane n = ");
	scanf("%d", &n);
	int i;
	struct point a[10000], temp;
	for (i = 0; i < n; ++ i){
		scanf("%d%d", &temp.x, &temp.y);
		a[i] = temp;
	}
	double max_dist = 0.0, cur_dist = 0.0;
	int j;
	for (i = 0; i < n; ++ i)
		for (j = 0; j < n; ++ j){
			cur_dist = dist(a[i], a[j]);
			if (cur_dist > max_dist)
				max_dist = cur_dist;
		}
	printf("%.10lf", max_dist);
	return 0;
}
