#include <stdio.h>
#include <stdlib.h>

int a[10000][10000];

void WriteMatrix(int n){
	int i, j;
	for (i = 0; i < n; ++ i){
		for (j = 0; j < n; ++ j)
			printf("%-7d ", a[i][j]);
		printf("\n");
	}
}

void GenMatrix(int n){
	int i = 0, j = 0, cur = 2, l = 0;
	while (cur <= n*n){
		for (l = j + 1; l < j + 2; ++ l){
			a[i][l] = cur;
			cur ++;
		}	
		j = -- l;
		i = 0;
		for (l = i + 1; l <= j; ++ l){
			a[l][j] = cur;
			cur ++;
		}
		i = --l;
		for (l = j - 1; l >= 0; -- l){
			a[i][l] = cur;
			cur ++;
		}
		j = 0;
		if (cur >= n*n)
			break;
		i ++;
		for (l = i; l < i + 1; ++ l){
			a[l][j] = cur;
			cur ++;
		}
		i = --l;
		for (l = j + 1 ; l <= i; ++ l){
			a[i][l] = cur;
			cur ++;
		}
		j = --l;
		for (l = i - 1; l >= 0; -- l){
			a[l][j] = cur;
			cur ++;
		}
		i = ++ l;
	}
}


int main(){
	int n;
	printf("Enter n ");
	scanf("%d", &n);
	int i, j;
	a[0][0] = 1;
	GenMatrix(n);
	WriteMatrix(n);
	return 0;
}
