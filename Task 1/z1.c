#include <stdio.h>
#include <stdlib.h>

double comput(double* coeff, int n, double x){
	double* cur_sum = (double*)malloc(sizeof(double) * n);
	cur_sum[0] = coeff[0];
	int i;
	for (i = 1; i <= n; ++i)
		cur_sum[i] = coeff[i] + x * cur_sum[i - 1];
	free(cur_sum);
	return cur_sum[n];
}

int main(){
	printf("Enter coordinates of real point ");
	double x;
	scanf("%lf", &x);
	printf("Enter degrees of polynom ");
	int n;
	scanf("%d", &n);
	double* coeff = (double*)malloc(sizeof(double) * n);
	
	printf("Enter the coefficients of the polynomial (in decreasing order of degree):\n");
	int i;
	for (i = 0; i <= n; ++i)
		scanf("%lf", &coeff[i]);
		
	double ans = 0;
	ans = comput(coeff, n, x);
	
	printf("%f\n", ans);
	free(coeff);
	return 0;
}