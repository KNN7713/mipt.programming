#include <stdio.h>

int main(){
	int n;
	scanf("%d", &n);
	int i = 0, x;
	int cur = 0, sum = 0, res = 0, left = 0, right = 0, minsum = 0, minpos = -1;
	for (i; i < n; ++ i){
		scanf("%d", &x);
		if (i == 0){
			sum = x;
			res = x;
		}
		sum += x;
		cur = sum - minsum;
		if (cur > res){
			res = cur;
			left = minpos + 1;
			right = i;
		}
		if (sum < minsum){
			minsum = sum;
			minpos = i;
		}
	}
	printf("The maximum sum is reached on subsegments i = %d, j = %d\n", left, right);
	printf("and equal %d", res);
	return 0;
}
