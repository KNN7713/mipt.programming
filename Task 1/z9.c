#include <stdio.h>
#include <math.h>

int main(){
	long long n;
	scanf("%ld", &n);
	long long i;
	double double_sum = 0.0;
	float float_sum = 0.0; 
	for (i = 1; i <= n; ++ i){
		double_sum += (double)1/(i * i);
		float_sum += (float)1/(i * i);
	}
	double exact_sum = (M_PI * M_PI) / 6;
	printf("exact_sum = %lf\n", exact_sum);
	printf("float_sum = %lf\n", float_sum);
	printf("double_sum = %lf", double_sum);
	return 0;
}
