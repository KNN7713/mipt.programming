#include <stdio.h>

int comp(const void * a, const void * b){
	return *(const int*)a > *(const int*)b; 
}

int main(){
	int n;
	int a[1000];
	scanf("%d", &n);
	int i;
	for (i = 0; i < n; i++)
		scanf("%d", &a[i]);
	qsort(a, n, sizeof(int), &comp);
	if (n % 2 == 0)
		printf("x = %d", a[(n / 2) - 1]);
	else
		printf("x = %d", a[((n + 1) / 2) - 1]);
	return 0;
}
