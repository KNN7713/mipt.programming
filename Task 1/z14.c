#include <stdio.h>
#include <math.h>

int binsearch_solve(double(*f)(double), double l, double r, double *x, double epsilon, unsigned max_it){
	double m = 0;
	int i = 0;
	while (fabs(r - l) > epsilon){
		i ++;
		m = (double) (r + l) / 2;
		if ((*f)(m) > 0)
			r = m;
		else
			l = m;
		if (i > max_it)
			break;
	}
	if (i <= max_it){
		*x = r;
		return 1;
	}
	else
		return 0;
}

double pol1(double x){
	return x * x - 2 * x + 1;
}	

int main(){
	double eps, a, b;
	unsigned int max_it;
	printf("Enter epsilon: ");
	scanf("%lf", &eps);
	printf("Enter the maximum number of iterations: ");
	scanf("%d", &max_it);
	printf("Enter the length of the border: ");
	scanf("%lf", &a);
	scanf("%lf", &b);
	double *x;
	int t = binsearch_solve(&pol1, a, b, x, eps, max_it);
	if (t != 0) 
		printf("%lf", x);
	else
		printf("SEARCH SOLUTION FAILED");
	return 0;
}
