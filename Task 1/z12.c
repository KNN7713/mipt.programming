#include <stdio.h>

void Gen(int * a, int n){
	int i;
	for (i = 0; i < n; ++ i)
		a[i] = random(10);
}

void Read(int * a, int n){
	int i;
	for (i = 0; i < n; ++ i)
		scanf("%d", &a[i]);
}

void ReadFile(int * a, int n, const char * FileName){
	FILE * f = fopen(FileName, "r");
	if (!f)
		return;
	int i;
	for (i = 0; i < n; ++ i)
		fscanf(f, "%d", a[i]);
	fclose(f);
}
	

void Write(int *a, int n){
	int i;
	for (i = 0; i < n; ++ i)
		printf("%d ", a[i]);
}

void WriteFile(int *a, int n, const char *FileName){
	FILE * f = fopen(FileName, "w");
	if (!f)
		return;
	int i;
	for (i = 0; i < n; ++ i)
		fprintf(f, "%d", a[i]);
	fclose(f);
}

void swap(int *a, int *b){
	int t;
	t = *a;
	*a = *b;
	*b = t;
}
		
void BubleSort(int *a, int n){
	int i, j;
	for (i = 0; i < n - 1; ++ i)
		for (j = 0; j < n - 1; ++ j)
			if (a[j] > a[j + 1])
				swap(&a[j], &a[j + 1]);
}

int main(){
	int n;
	scanf("%d", &n);
	int* a = (int *)malloc(sizeof(int) * n);	
	//Gen(a, n);
	Read(a, n);
	//ReadFile(a, n);
	BubleSort(a, n);
	Write(a, n);
	free(a);
	return 0;
}
